// ------------------------------------------------------------------------
const Discord = require("discord.js");
const fs = require("fs");
const config = require("./config.json");
const client = new Discord.Client({ intents: ["GUILDS", "GUILD_MESSAGES"] });
const prefix = "!";

// ------------------------------------------------------------------------
// sur bot ready
client.on("ready", () =>{
    console.log(`Logged in as ${client.user.tag}!`);
});

// ------------------------------------------------------------------------
client.on("message", function(message) {
  if (message.author.bot) return;
    
// ------------------------------------------------------------------------
 // Variables génériques 
const commandBody = message.content.slice(prefix.length);
const sender = message.author;
const args = commandBody.split(' ');
const command = args.shift().toLowerCase();

// ------------------------------------------------------------------------
// Gestion des niveaux
var userData = JSON.parse(fs.readFileSync('jsonPointID/level.json', 'utf-8'));   

if(command === "stats") {
    message.channel.send('Vous avez envoyé **' + userData[sender.id].messagesSent + '** messages !' );
}
 
if (!userData[sender.id]) userData[sender.id] = {
    messageSent : 1
};
userData[sender.id].messagesSent++;
    
fs.writeFile('jsonPointID/level.json', JSON.stringify(userData), (err) => {
    if (err) console.error(err);
});
    
// ------------------------------------------------------------------------
// Gestion envoi message twitch prime one shot via !prime
  if (command === "prime") {    
  	const channel = client.channels.cache.find(channel => channel.name === "league_of_legends");
    channel.send({
    	 "tts": false,
    	 "embeds": [
    	 {
       		"type": "rich",
       		"title": `Récupérez votre capsule prime sur LoL`,
       		"description": `On est en début de mois, pensez a récupérer votre capsule prime gaming. `,
       		"color": 0x00FFFF,
       		"image": {
         		"url": `https://www.team-aaa.com/upload/media/post_image/0001/07/99ffe77927f0312ece398c9674d63dd9653e4ff5.jpeg`,
          		"height": 0,
           		"width": 0
       		},
       		"url": `https://gaming.amazon.com/loot/lol10?ingress=amzn&ref_=SM_LOLS10_P5_CRWN`
     	}]
    });
  }
  
  if (command === "test") {    
  	const channel = client.channels.cache.find(channel => channel.name === "test");
    channel.send('Test')
    });
  }
  
});

// ------------------------------------------------------------------------
// lancement du Bot
client.login(config.BOT_TOKEN);

// ------------------------------------------------------------------------
// Gestion envoi message twitch prime tous les mois
const RappelCapsulePrimeLoL = () => {
    const d = new Date();
    const Day = d.getDate();
    const Heure = d.getHours();
    const Minutes = d.getMinutes();
    
    if (Day === 1 && Heure == 18 && Minutes === 0)
    {
        const channel = client.channels.cache.find(channel => channel.name === "league_of_legends");
        channel.send({
     		 "tts": false,
     		 "embeds": [
       		 {
         		"type": "rich",
          		"title": `Récupérez votre capsule prime sur LoL`,
          		"description": `On est en début de mois, pensez a récupérer votre capsule prime gaming. `,
          		"color": 0x00FFFF,
          		"image": {
            		"url": `https://www.team-aaa.com/upload/media/post_image/0001/07/99ffe77927f0312ece398c9674d63dd9653e4ff5.jpeg`,
            		"height": 0,
            		"width": 0
          		},
          		"url": `https://gaming.amazon.com/loot/lol10?ingress=amzn&ref_=SM_LOLS10_P5_CRWN`
        	}]
    	});
    }
};

setInterval(RappelCapsulePrimeLoL, 60000);
// ------------------------------------------------------------------------


